import csv
from datetime import datetime
import os
import locale
from collections import OrderedDict
import xlsxwriter
import importlib.util
import sys
#from pip._internal import main 

#Verifica se o xlsx2csv esta instalado para a conversao do xlsx to csv
package_name = 'xlsx2csv'
package_name2 = 'xlsxwriter'
#spec = importlib.util.find_spec(package_name)
#if spec is None:
#    main(['install', package_name])



path = input("Digite o nome do arquivo: ")

#Fazer com que o strptime aceitei o padrão Brasileiro referente a abreviação do Mês 
locale.setlocale(locale.LC_TIME, 'pt_BR.utf8')

#os.rename(path, path+'.bkp')
fread = open(path, 'r')
title = ['Tipo de item','Chave da item','ID da item', 'Prioridade', 'Status', 'Criado', 'Resumo', 'Campo personalizado (Meet SLA)']
array = []
with open(path, 'r') as file:
    reader = csv.DictReader(file, fieldnames=title)
    #reader = f.readlines()
    #print(reader)
    cont = 0
    for row in reader:
        for k, v in row.items():
            if k == "Criado":
                if v == "Criado":
                    temp = [k, v]
                    array.append(temp)
                    x = 0
                else:
                    data, hrs, secs = v.split(' ')
                    convert = datetime.strptime(data, '%d/%b/%y')
                    #Separar a data da hora depois de converter
                    new_date, hrs = '{}'.format(convert).split(' ')
                    #Salvar os dados em um array de lista        
                    v = new_date
                    temp = [k, v]
                    array.append(temp)
                    
            else:
                temp = [k, v]
                array.append(temp)


name, exten = path.split('.')
#Gerar o Excel
file_name = name + '.xlsx'
workbook = xlsxwriter.Workbook(file_name)
worksheet = workbook.add_worksheet()
cont = 0
row = 0
col = 0 
tamanho = int(len(array))
while cont < tamanho:
    x = array[cont][0]
    y = array[cont][1]
    if col < 7:
        worksheet.write(row, col,y)
        col += 1
    else:
        row += 1
        col = 0
    cont += 1
workbook.close()

#Converter para csv
convert_file = 'xlsx2csv -s 0 {} {}'.format(file_name, name) 
os.system(convert_file)