#!/bin/bash

# Script feito para a verificação da Latência de URL
# Para testar a latencia de uma url especifica 
# executar ./latencia-v2.sh www.google.com.br 
# Caso não passe nenhum url, ele irar fazer o teste das URLs da MaxiPago
# 02/07/2018

url=$1
if [ ! -z "$url" ]; then
    echo "Testando url " $url
    for X in 'seq 60'; do curl -Ik -w "HTTPCode=%{http_code} TotalTime=%{time_total}\n" $url -so /dev/null; done    
else
    echo "Testando Portal MaxiPago"
    for X in 'seq 60'; do curl -Ik -w "HTTPCode=%{http_code} TotalTime=%{time_total}\n" https://portal.maxipago.net/vpos/docs/maxipagologin.html -so /dev/null; done
    echo ""
    echo "Testando API MaxiPago"
    for X in 'seq 60'; do curl -X POST -Ik -w  "HTTPCode=%{http_code} TotalTime=%{time_total}\n" https://api.maxipago.net//UniversalAPI/postAPI -so /dev/null; done
    echo ""
    echo "Testando API XML MaxiPago"
    for X in 'seq 60'; do curl -X POST -Ik -w  "HTTPCode=%{http_code} TotalTime=%{time_total}\n" https://api.maxipago.net//UniversalAPI/postXML -so /dev/null; done
    echo ""
fi


