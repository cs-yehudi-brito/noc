#!/bin/bash

#Instalar o GIT
#lsb_release -i

#Configurando a Pasta do Repositorio
echo -e "Qual a url do seu repositorio "
read repo
echo -e "Digite o caminho que voce quer para backup "
read caminho
cd caminho 
echo "Clonando seu repositorio e fazendo as configuracoes"
git clone $repo


#pegando o nome da pasta
IFS='/' read -r -a array <<< $repo
x=${array[4]}
IFS2='.' read -r -a array2 <<< $x
git_caminho = $array2
echo $git_caminho

#Entrando na pasta do repositorio
cd $caminho/$git_caminho
echo pwd
git init
git config credential.helper store


#Criando o Script
cd /home/$USER
echo -e "date=`date '+%d-%m-%Y %H:%M'`" >> script_logout.sh
echo -e "cd $caminho/$git_caminho" >> script_logout.sh 
echo -e "git add ." >> script_logout.sh
echo -e "git commit -m 'Update - $date'" >> script_logout.sh
echo -e "git push origin master" >> script_logout.sh
`chmod + 700 script_logout.sh`


#Editar o arquivo .bashrc
echo -e "exit_session () {" >> teste
echo -e " . $HOME/.bash_logout" >> teste
echo -e "}" >> teste
echo -e "trap exit_session EXIT" >> teste

#Editar o arquivo bash_profile
echo -e "/bin/bash /home/$USER/Documents/script_logout.sh" >> teste
